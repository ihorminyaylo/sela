package com.greeting.model;

import javax.persistence.*;

@Entity
public class Card extends AbstractEntity{


    @Column(nullable = false)
    private String text;

    @ManyToOne
    @JoinColumn(name = "template_id")
    private Template template;

    public Template getTemplate() {
        return template;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    public void setTemplate(Template template) {
        this.template = template;
    }
}
