package com.greeting.service;

import com.greeting.dao.GreetingCardDao;
import com.greeting.exception.BusinnesException;
import com.greeting.model.Card;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class GreetingCardService {

    @Autowired
    private GreetingCardDao greetingCardDao;

    @Autowired
    public GreetingCardService(GreetingCardDao greetingCardDao) {
        this.greetingCardDao = greetingCardDao;
    }

    public Card create(Card card) throws BusinnesException {
        String text = card.getTemplate().getText();
        if (text.contains("{}")) {
            throw new BusinnesException("Please write your information on required fields");
        }
        return greetingCardDao.create(card);
    }
}
