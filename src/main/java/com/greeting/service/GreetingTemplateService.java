package com.greeting.service;

import com.greeting.dao.GreetingTemplateDao;
import com.greeting.model.Template;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class GreetingTemplateService {

    @Autowired
    private GreetingTemplateDao greetingTemplateDao;

    @Autowired
    public GreetingTemplateService(GreetingTemplateDao greetingTemplateDao) {
        this.greetingTemplateDao = greetingTemplateDao;
    }

    public Template create(Template template) {
        return greetingTemplateDao.create(template);
    }

    public List<Template> getAll() {
        return greetingTemplateDao.getAll();
    }
}
