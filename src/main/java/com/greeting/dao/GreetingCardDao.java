package com.greeting.dao;

import com.greeting.model.Card;
import org.springframework.stereotype.Repository;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

@Repository
public class GreetingCardDao {

    @PersistenceContext
    private EntityManager entityManager;

    public Card create(Card card) {
        entityManager.persist(card);
        return card;
    }
}
