package com.greeting.dao;

import com.greeting.model.Template;
import org.springframework.stereotype.Repository;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import java.util.List;

@Repository
public class GreetingTemplateDao {

    @PersistenceContext
    private EntityManager entityManager;

    public Template create(Template template) {
        entityManager.persist(template);
        return template;
    }

    public List<Template> getAll() {
        Query query = entityManager.createNativeQuery("SELECT * FROM template", Template.class);
        return query.getResultList();
    }
}
