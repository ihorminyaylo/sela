package com.greeting;

import com.greeting.controller.GreetingCardController;
import com.greeting.dao.GreetingCardDao;
import com.greeting.model.Template;
import com.greeting.service.GreetingCardService;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.ComponentScan;

@SpringBootApplication
public class GreetingcardApplication {

	public static void main(String[] args) {
		SpringApplication.run(GreetingcardApplication.class, args);
	}
}
