package com.greeting.controller;

import com.greeting.exception.BusinnesException;
import com.greeting.model.Card;
import com.greeting.model.Template;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import com.greeting.service.GreetingCardService;
import com.greeting.service.GreetingTemplateService;

import java.util.List;

@RestController
@RequestMapping(value = "/api/greetingCard")
public class GreetingCardController {

    private final GreetingTemplateService greetingTemplateService;

    private final GreetingCardService greetingCardService;

    @Autowired
    public GreetingCardController(GreetingTemplateService greetingTemplateService, GreetingCardService greetingCardService) {
        this.greetingTemplateService = greetingTemplateService;
        this.greetingCardService = greetingCardService;
    }

    @GetMapping(value = "/catalog")
    public ResponseEntity<List<Template>> getAll() {
        return ResponseEntity.ok(greetingTemplateService.getAll());
    }

    @PutMapping
    public ResponseEntity createTempate(@RequestBody Template template) {
        return ResponseEntity.ok(greetingTemplateService.create(template));
    }


    @PostMapping
    public ResponseEntity createCard(@RequestBody Card card) throws BusinnesException {
        return ResponseEntity.ok(greetingCardService.create(card));
    }
}
